'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class HardwareTypesSchema extends Schema {
  up () {
    this.create('hardware_types', (table) => {
      table.increments()
      table.string("name")
      table.string("interactor")
      table.string("icon")
      table.timestamps()
    })
  }

  down () {
    this.drop('hardware_types')
  }
}

module.exports = HardwareTypesSchema
