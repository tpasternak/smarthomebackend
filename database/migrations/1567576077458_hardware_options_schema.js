'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class HardwareOptionsSchema extends Schema {
  up () {
    this.create('hardware_options', (table) => {
      table.increments()
      table.integer("hardware_type_id")
      table.string("option")
      table.timestamps()
    })
  }

  down () {
    this.drop('hardware_options')
  }
}

module.exports = HardwareOptionsSchema
