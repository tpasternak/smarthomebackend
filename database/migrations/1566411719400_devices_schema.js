'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class DevicesSchema extends Schema {
  up () {
    this.create('devices', (table) => {
      table.increments()
      table.string("name")
      table.string("address")
      table.integer("hardware_types_id")
      table.integer("rooms_id")
      table.string("tile_size")
      table.boolean("favorite")
      table.timestamps()
    })
  }

  down () {
    this.drop('devices')
  }
}

module.exports = DevicesSchema
