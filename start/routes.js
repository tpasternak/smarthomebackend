'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')
const Database = use('Database')



Route.get('/', 'PageController.index')
Route.get('test/shutdown', 'TestController.shutdown')
Route.get('test/start', 'TestController.start')

Route.get("/devices/:id/", 'PageController.getDeviceDetails')
Route.get("/devices/:id/on", 'PageController.startDevice')
Route.get("/devices/:id/off", 'PageController.shutdownDevice')
Route.get("/devices/:id/change/:attribute/:value", 'PageController.changeAttribute')

Route.get("/devices/", "PageController.getAllDevices")
Route.get("/favorites/", "FavoriteController.index")