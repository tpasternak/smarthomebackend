'use strict'
const DeviceInteractorFile = require("../Hardware/DeviceInteractor")
const Devices =  use("App/Models/Device")
class PageController {

    constructor() {
        this.deviceInteractor = new DeviceInteractorFile()

    }

    index() {
        return []
    }

    async getAllDevices({request, response, view}) {
        let devices = await Devices.all()
        return devices
    }


    async startDevice({request, response, params}) {
        return this.deviceInteractor.turnOnDevice(params.id)



    }

    async shutdownDevice({request, response, params}) {
        this.deviceInteractor.turnOffDevice(params.id)

        return {
            status: "OK",
            msg: "Device turned off!",
        }

    }


    async getDeviceDetails({request, response, params}) {

        let device = await Devices.getFullDetail(params.id)
        return device;

    }
}

module.exports = PageController
