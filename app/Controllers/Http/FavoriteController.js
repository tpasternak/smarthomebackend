'use strict'
const Device =  use("App/Models/Device")

class FavoriteController {
    async index() {
        let favoriteDevices = await Device.getFavorite()
        return favoriteDevices;
    }
}

module.exports = FavoriteController
