'use strict'

const DeviceInteractorFile = require("../Hardware/DeviceInteractor")
class TestController {

    constructor() {
        this.deviceInteractor = new DeviceInteractorFile()
    }

    index({request, response, view}) {
        return view.render("welcome")
    }

    shutdown ({ request, response, view}) {
        this.deviceInteractor.turnOffDevice(1)
        return "Device turned off!"

    }

    start ({ request, response, view}) {


        this.deviceInteractor.turnOnDevice(1)

        return "Device turned on!"

    }

}


module.exports = TestController
