'use strict'
const Devices =  use("App/Models/Device")
const DeviceInteractorFile = require("../Hardware/DeviceInteractor")

class DeviceController {
  constructor ({ socket, request }) {
    this.socket = socket
    this.request = request
    this.deviceInteractor = new DeviceInteractorFile()
  }

  async onrequestDeviceStatus(message) {

    let device = await Devices.getFullDetail(message.device)

    this.socket.emit("responseDeviceStatus", device)

  }

  async onstartDevice(message) {
    let response = await this.deviceInteractor.turnOnDevice(message.device)
    this.socket.emit("actionSuccessful", response)
    this.onrequestDeviceStatus(message)
  }

  async onstopDevice(message) {
    let response = await this.deviceInteractor.turnOffDevice(message.device)
    this.socket.emit("actionSuccessful", response)
    this.onrequestDeviceStatus(message)

  }

  async onrestartDevice(message) {
    let response = await this.deviceInteractor.turnOffDevice(message.device)
    this.socket.emit("actionSuccessful", response)

    response = await this.deviceInteractor.turnOnDevice(message.device)
    this.socket.emit("actionSuccessful", response)

    this.onrequestDeviceStatus(message)

  }
}

module.exports = DeviceController
