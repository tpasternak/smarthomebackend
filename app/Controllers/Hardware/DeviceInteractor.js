'use strict'
const sp = require('synchronized-promise')

class DeviceInteractor {


    constructor() {
        this.device = use("App/Models/Device")
    }

    async turnOnDevice(id) {

        let device = await this.getDeviceDetails(id)
        let Ability = await this.device.hasAbility(id, "powerOn");
        if(Ability) {

            device.turnOn()
            return {status: "ok", message: "Device turned on!"}

        }


        return {status: "error", message: "Device cant be turned on!"}
    }

    async turnOffDevice(id) {
        let device = await this.getDeviceDetails(id)
        let Ability = await this.device.hasAbility(id, "powerOff");
        if(Ability) {

            device.turnOff()
            return {status: "ok", message: "Device turned off!"}

        }


        return {status: "error", message: "Device cant be turned off!"}

    }

    async getDeviceDetails(id) {

        let device = await this.device.find(id)
        let hardwareType = await device.getHardwareType()

        let DeviceAPI = require("./"+hardwareType.interactor)
        this.interactor = device.hardware_type
        return new DeviceAPI(device);

    }

}

module.exports = DeviceInteractor