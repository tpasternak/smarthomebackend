'use strict'

class YamahaReceiver {

    constructor(device) {
        let YamahaAPI = require("yamaha-nodejs")
        this.interactor = new YamahaAPI(device.address)

    }


    turnOn() {
        return this.interactor.powerOn().then(function(){

            return "ok"

        });
    }
    turnOff() {
        return this.interactor.powerOff().then(function(){

            return "ok"

        });
    }

    changeAttribute(attribute, value) {

    }
}

module.exports = YamahaReceiver