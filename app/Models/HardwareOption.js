'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class HardwareOption extends Model {

    getHardwareTypes () {
        return this.hasMany("App/Models/HardwareType")
    }

    static get hidden() {
        return ["hardware_type_id", "created_at", "updated_at"]
    }
}

module.exports = HardwareOption
