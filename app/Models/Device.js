'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Device extends Model {

    getRoom () {
        return this.hasOne("App/Models/Room", "rooms_id", "id").fetch()
    }

    getHardwareType () {
        return this.hasOne("App/Models/HardwareType", "hardware_types_id", "id").fetch()
    }


    static get hidden () {
        return ["rooms_id", "hardware_types_id"]
    }

    static getFavorite () {
        return this.query().where('favorite', 1).fetch();
    }

    static async getFullDetail(id) {

        let device = await this.find(id)
        let rooms = await device.getRoom()
        let hardwareType = await device.getHardwareType()
        let options = await hardwareType.getOptions().fetch()

        return {
            device: device,
            room: rooms,
            hardwareType: hardwareType,
            options: options
        }
    }

    static async hasAbility(id, ability) {
        let device = await this.find(id)
        let hardwareType = await device.getHardwareType()
        let options = await hardwareType.getOptions().fetch()
        let hasAbility = false
            for(let i in options.rows) {
                if(options.rows[i].option === ability) {
                    hasAbility = true

                }
            }

        return hasAbility;
    }
}

module.exports = Device
