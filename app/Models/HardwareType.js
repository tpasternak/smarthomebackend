'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class HardwareType extends Model {

    getOptions () {
        return this.hasMany("App/Models/HardwareOption")
    }

    getDevices () {
        return this.hasMany("App/Models/Device")
    }
}

module.exports = HardwareType
